/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"fmt"
	"image"
	"io/ioutil"
	"runtime"
	"strconv"
)

type Benchmark struct {
}

type IBenchmark interface {
	createBenchmark(diffuser IDiffuser, img image.Image, filePath string)
}

// Runs all matrices with differnt amount of cores (starting from 1 to max number) and creates a CSV
func (b Benchmark) CreateBenchmark(diffuser IDiffuser, img image.Image, filePath string) {
	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	// Gray scale
	StartStopWatch()
	grayImg := GenerateGrayScaleImage(img)
	StopStopWatch()

	fmt.Print("Duration of generating grayscale: ")
	fmt.Print(elapsedStopWatchTime.Seconds() * 1000)
	fmt.Println(" ms\n")

	SaveImage(grayImg, filePath, "grayscale")

	var chart string
	chart += "Floyd-Steinberg\n"
	chart += "Cores;Time in ms\n"

	// Floyd
	var imgArrayFloyd [][]int

	for i := 1; i <= runtime.NumCPU(); i++ {
		runtime.GOMAXPROCS(i)
		imgArrayFloyd = ImageToTwoDimArray(grayImg)

		StartStopWatch()
		diffuser.Diffuse(imgArrayFloyd, floydMatrix, rows, columns)
		StopStopWatch()

		floydDuration := elapsedStopWatchTime.Seconds() * 1000
		floydDurationString := strconv.FormatFloat(floydDuration, 'f', 0, 64)
		numberOfCoresString := strconv.FormatInt(int64(i), 16)
		chart += numberOfCoresString + ";" + floydDurationString + "\n"

		fmt.Printf("Duration of Floyd-Steinberg-Algorithm with %d cores: ", i)
		fmt.Print(floydDuration)
		fmt.Println(" ms\n")
	}

	SaveImage(TwoDimArrayToImage(imgArrayFloyd), filePath, "floyd")

	chart += "\n"
	chart += "Stucki\n"
	chart += "Cores;Time in ms\n"

	// Stucki
	var imgArrayStucki [][]int

	for i := 1; i <= runtime.NumCPU(); i++ {
		runtime.GOMAXPROCS(i)
		imgArrayStucki = ImageToTwoDimArray(grayImg)

		StartStopWatch()
		diffuser.Diffuse(imgArrayStucki, stuckiMatrix, rows, columns)
		StopStopWatch()

		stuckiDuration := elapsedStopWatchTime.Seconds() * 1000
		stuckiDurationString := strconv.FormatFloat(stuckiDuration, 'f', 0, 64)
		numberOfCoresString := strconv.FormatInt(int64(i), 16)
		chart += numberOfCoresString + ";" + stuckiDurationString + "\n"

		fmt.Printf("Duration of Stucki-Algorithm with %d cores: ", i)
		fmt.Print(stuckiDuration)
		fmt.Println(" ms\n")
	}

	SaveImage(TwoDimArrayToImage(imgArrayStucki), filePath, "stucki")

	chart += "\n"
	chart += "Own\n"
	chart += "Cores;Time in ms\n"

	// Own
	var imgArrayOwn [][]int

	for i := 1; i <= runtime.NumCPU(); i++ {
		runtime.GOMAXPROCS(i)
		imgArrayOwn = ImageToTwoDimArray(grayImg)

		StartStopWatch()
		diffuser.Diffuse(imgArrayOwn, ownMatrix, rows, columns)
		StopStopWatch()

		ownDuration := elapsedStopWatchTime.Seconds() * 1000
		ownDurationString := strconv.FormatFloat(ownDuration, 'f', 0, 64)
		numberOfCoresString := strconv.FormatInt(int64(i), 16)
		chart += numberOfCoresString + ";" + ownDurationString + "\n"

		fmt.Printf("Duration of own algorithm with %d cores: ", i)
		fmt.Print(ownDuration)
		fmt.Println(" ms\n")
	}

	SaveImage(TwoDimArrayToImage(imgArrayOwn), filePath, "own")

	chartBytes := []byte(chart)
	_, fileDir, _, fileName := GetFilePathInfo(filePath)
	chartFilePath := fileDir + fileName + "_chart.csv"
	ioutil.WriteFile(chartFilePath, chartBytes, 0644)
	fmt.Print("Saving benchmark to " + chartFilePath + "\n\n")
}
