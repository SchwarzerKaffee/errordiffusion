/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetColumnDifference(t *testing.T) {
	// Prepare & Execute
	floydDifference := GetColumnDifference(floydMatrix)
	stuckiDifference := GetColumnDifference(stuckiMatrix)
	ownDifference := GetColumnDifference(ownMatrix)

	fantasyMatrix := [][]float64{}
	fantasyMatrix = append(fantasyMatrix, []float64{2, 2, 2})
	noDifference := GetColumnDifference(fantasyMatrix)

	// Verify
	assert.Equal(t, 1, floydDifference)
	assert.Equal(t, 2, stuckiDifference)
	assert.Equal(t, 1, ownDifference)
	assert.Equal(t, -1, noDifference)
}
