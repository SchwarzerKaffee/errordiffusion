/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

// Values of 0 will be ignored, Value of 1 is the pixel to be diffused

var floydMatrix = [][]float64{{0, 1, 7. / 16.}, {3. / 16., 5. / 16., 1. / 16.}}

var ownMatrix = [][]float64{{0, 1, 4. / 12.}, {1. / 12., 4. / 12., 1. / 12.}, {0, 1. / 12., 0}}

var stuckiMatrix = [][]float64{{0, 0, 1, 8. / 42., 4. / 42.}, {2. / 42., 4. / 42., 8. / 42., 4. / 42., 2. / 42.}, {1. / 42., 2. / 42., 4. / 42., 2. / 42., 1. / 42.}}

// Return the index of the pixel (column) to be diffused
func GetColumnDifference(matrix [][]float64) int {
	return GetPositionOf(1, matrix[0])
}

// Return the index of the value in a slice
func GetPositionOf(value float64, slice []float64) int {
	for index, item := range slice {
		if item == value {
			return index
		}
	}

	return -1
}
