/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"bufio"
	"fmt"
	"image"
	"os"
	"runtime"
)

// Let the user choose a filepath
func RequestFilePath() string {
	stdin := bufio.NewReader(os.Stdin)
	fmt.Println("Which file (.jpg/.jpeg or .png) do you wanna process?")

	var filePath string
	fmt.Fscan(stdin, &filePath)
	fmt.Println("")
	Flush(stdin)

	return filePath
}

// Let the user choose an action
func RequestAction() string {
	stdin := bufio.NewReader(os.Stdin)

	fmt.Println("Which action do you wanna run?")
	fmt.Println(" [0] Serial Floyd-Steinberg")
	fmt.Println(" [1] Parallel Floyd-Steinberg")
	fmt.Println(" [2] Serial Stucki")
	fmt.Println(" [3] Parallel Stucki")
	fmt.Println(" [4] Serial Own")
	fmt.Println(" [5] Parallel Own")
	fmt.Println(" [b] Create benchmark (<filename>_chart.csv)")
	fmt.Println(" [x] Exit")

	var action string
	fmt.Fscan(stdin, &action)
	fmt.Println("")
	Flush(stdin)

	return action
}

// Flushes the stdio input buffer
func Flush(reader *bufio.Reader) {
	var i int
	for i = 0; i < reader.Buffered(); i++ {
		reader.ReadByte()
	}
}

// Display menu to user and apply actions
func Menu() {
	var benchmark Benchmark
	var diffuser Diffuser
	action := RequestAction()

	switch action {
	case "0":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			procImg := RunSerial(img, filePath, floydMatrix)
			SaveImage(procImg, filePath, "floydserial")
		}
	case "1":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			procImg := RunParallel(img, filePath, floydMatrix)
			SaveImage(procImg, filePath, "floydparallel")
		}
	case "2":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			procImg := RunSerial(img, filePath, stuckiMatrix)
			SaveImage(procImg, filePath, "stuckiserial")
		}
	case "3":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			procImg := RunParallel(img, filePath, stuckiMatrix)
			SaveImage(procImg, filePath, "stuckiparallel")
		}
	case "4":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			procImg := RunSerial(img, filePath, ownMatrix)
			SaveImage(procImg, filePath, "ownserial")
		}
	case "5":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			procImg := RunParallel(img, filePath, ownMatrix)
			SaveImage(procImg, filePath, "ownparallel")
		}
	case "b":
		filePath := RequestFilePath()
		img, err := LoadImage(filePath)

		if err != nil {
			fmt.Println("Invalid file path.")
			break
		} else {
			benchmark.CreateBenchmark(diffuser, img, filePath)
		}
	case "x":
		os.Exit(0)
	default:
		fmt.Println("What? Operation not supported...")
	}
}

func main() {
	for {
		Menu()
	}
}

// Run the specified matrix with one core
func RunSerial(img image.Image, filePath string, matrix [][]float64) image.Image {
	runtime.GOMAXPROCS(1)
	var diffuser Diffuser

	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	// Gray scale
	StartStopWatch()
	grayImg := GenerateGrayScaleImage(img)
	StopStopWatch()

	fmt.Print("Duration of generating grayscale: ")
	fmt.Print(elapsedStopWatchTime.Seconds() * 1000)
	fmt.Println(" ms\n")

	SaveImage(grayImg, filePath, "grayscale")

	imgArray := ImageToTwoDimArray(grayImg)

	StartStopWatch()
	diffuser.Diffuse(imgArray, matrix, rows, columns)
	StopStopWatch()

	serialDuration := elapsedStopWatchTime.Seconds() * 1000

	fmt.Printf("Duration of serial algorithm: ")
	fmt.Print(serialDuration)
	fmt.Println(" ms\n")

	return TwoDimArrayToImage(imgArray)
}

// Run the specified matrix with max possible core
func RunParallel(img image.Image, filePath string, matrix [][]float64) image.Image {
	runtime.GOMAXPROCS(runtime.NumCPU())
	var diffuser Diffuser

	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	// Gray scale
	StartStopWatch()
	grayImg := GenerateGrayScaleImage(img)
	StopStopWatch()

	fmt.Print("Duration of generating grayscale: ")
	fmt.Print(elapsedStopWatchTime.Seconds() * 1000)
	fmt.Println(" ms\n")

	SaveImage(grayImg, filePath, "grayscale")

	imgArray := ImageToTwoDimArray(grayImg)

	StartStopWatch()
	diffuser.Diffuse(imgArray, matrix, rows, columns)
	StopStopWatch()

	parallelDuration := elapsedStopWatchTime.Seconds() * 1000

	fmt.Printf("Duration of parallel algorithm with %d cores: ", runtime.NumCPU())
	fmt.Print(parallelDuration)
	fmt.Println(" ms\n")

	return TwoDimArrayToImage(imgArray)
}
