/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import "time"

var begin time.Time
var elapsedStopWatchTime time.Duration

func StartStopWatch() {
	begin = time.Now()
}

func StopStopWatch() {
	elapsedStopWatchTime = time.Since(begin)
}
