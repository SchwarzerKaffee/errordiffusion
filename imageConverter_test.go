/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"image/color"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetGrayValue(t *testing.T) {
	// Prepare
	col := color.RGBA64{256 * 4, 256, 256, 256}
	col2 := color.RGBA64{256, 256, 256, 256}

	// Execute
	result := GetGrayValue(col)
	result2 := GetGrayValue(col2)

	// Verify
	assert.Equal(t, 4, result)
	assert.Equal(t, 1, result2)
}

func TestImageToTwoDimArray(t *testing.T) {
	// Prepare
	img, _ := LoadImage("testfile/test.jpg")

	// Execute
	imgArray := ImageToTwoDimArray(img)

	// Verify
	assert.Equal(t, img.Bounds().Max.X, len(imgArray))
	assert.Equal(t, img.Bounds().Max.Y, len(imgArray[0]))
	assert.Equal(t, 253, imgArray[0][0])
	assert.Equal(t, 0, imgArray[2][102])
	assert.Equal(t, 0, imgArray[42][42])
}

func TesttwoDimArrayToImage(t *testing.T) {
	// Prepare
	imgArray := [][]int{}

	row1 := []int{1, 2, 3}
	row2 := []int{4, 5, 6}
	row3 := []int{7, 8, 9}

	imgArray = append(imgArray, row1)
	imgArray = append(imgArray, row2)
	imgArray = append(imgArray, row3)

	// Execute
	img := TwoDimArrayToImage(imgArray)

	// Verify
	assert.Equal(t, len(imgArray), img.Bounds().Max.X)
	assert.Equal(t, len(imgArray[0]), img.Bounds().Max.Y)
	assert.Equal(t, 253, img.At(0, 0))
	assert.Equal(t, 0, img.At(2, 102))
	assert.Equal(t, 0, img.At(42, 42))
}
