/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"os"
	"path/filepath"
)

// Return file info of desired filepath
func GetFilePathInfo(filePath string) (fileBase string, fileDir string, fileExt string, fileName string) {
	fileBase = filepath.Base(filePath)
	fileDir = filepath.Dir(filePath) + "\\"
	fileExt = filepath.Ext(filePath)
	fileName = fileBase[:len(fileBase)-len(fileExt)]

	return fileBase, fileDir, fileExt, fileName
}

// Load image from filepath and return it
func LoadImage(path string) (image.Image, error) {
	file, err := os.Open(path)

	if err == nil {
		defer file.Close()

		var img image.Image = nil
		fileExt := filepath.Ext(path)

		switch fileExt {
		case ".jpeg":
			img, _ = jpeg.Decode(file)
		case ".jpg":
			img, _ = jpeg.Decode(file)
		case ".png":
			img, _ = png.Decode(file)
		default:
			return nil, errors.New("Ja Junge, ich raff dat net.")
		}

		return img, nil
	}

	return nil, err
}

// Save image to filepath with the possibility of adding an additional string to the file name
func SaveImage(img image.Image, filepath string, additionalName string) {
	_, fileDir, fileExt, fileName := GetFilePathInfo(filepath)

	var jpegOption jpeg.Options
	jpegOption.Quality = 100

	filePath := fileDir + fileName + "_" + additionalName + fileExt

	fmt.Print("Saving image to " + filePath + "\n\n")

	imgFile, _ := os.Create(filePath)

	switch fileExt {
	case ".jpeg":
		jpeg.Encode(imgFile, img, &jpegOption)
	case ".jpg":
		jpeg.Encode(imgFile, img, &jpegOption)
	case ".png":
		png.Encode(imgFile, img)
	}

	imgFile.Close()
}
