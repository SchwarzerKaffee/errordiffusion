/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStartStopWatch(t *testing.T) {
	// Prepare & Execute & Verify
	currentBegin := begin
	StartStopWatch()
	assert.NotEqual(t, currentBegin, begin)
	StopStopWatch()
}

func TestStopStopWatch(t *testing.T) {
	// Prepare
	StartStopWatch()

	// Execute & Verify
	currentElapsedStopWatchTimeNano := elapsedStopWatchTime.Nanoseconds()
	assert.Equal(t, 0, int(currentElapsedStopWatchTimeNano))
	StopStopWatch()
	assert.NotEqual(t, nil, elapsedStopWatchTime)
}
