/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetFilePathInfo(t *testing.T) {
	// Prepare
	filePath := "C:\\randombullcrap.jpg"
	filePath2 := "C:\\test\\randombullcrap42.png"

	// Execute
	base, dir, ext, name := GetFilePathInfo(filePath)
	base2, dir2, ext2, name2 := GetFilePathInfo(filePath2)

	// Verify
	assert.Equal(t, "randombullcrap.jpg", base)
	assert.Equal(t, "C:\\\\", dir)
	assert.Equal(t, ".jpg", ext)
	assert.Equal(t, "randombullcrap", name)

	assert.Equal(t, "randombullcrap42.png", base2)
	assert.Equal(t, "C:\\test\\", dir2)
	assert.Equal(t, ".png", ext2)
	assert.Equal(t, "randombullcrap42", name2)
}

func TestLoadImage(t *testing.T) {
	// Prepare & Execute
	img, _ := LoadImage("testfile/test.jpg")
	img2, err := LoadImage("testfile/menoexists.jpg")
	img3, _ := LoadImage("testfile/test.jpeg")
	img4, _ := LoadImage("testfile/test.png")

	// Verify
	assert.Equal(t, 552, img.Bounds().Max.X)
	assert.Equal(t, 384, img.Bounds().Max.Y)
	assert.Equal(t, 552, img3.Bounds().Max.X)
	assert.Equal(t, 384, img3.Bounds().Max.Y)
	assert.Equal(t, 552, img4.Bounds().Max.X)
	assert.Equal(t, 384, img4.Bounds().Max.Y)

	assert.Equal(t, nil, img2)
	assert.NotEqual(t, nil, err)
}

func TestSaveImage(t *testing.T) {
	// Prepare & Execute
	img, _ := LoadImage("testfile/test.jpg")
	img2, _ := LoadImage("testfile/test.jpeg")
	img3, _ := LoadImage("testfile/test.png")
	SaveImage(img, "testfile/created.jpg", "magic")
	SaveImage(img2, "testfile/created.jpeg", "magic")
	SaveImage(img3, "testfile/created.png", "magic")

	// Verify
	if _, err := os.Stat("testfile/created_magic.jpg"); os.IsNotExist(err) {
		assert.Equal(t, true, false)
	} else {
		assert.Equal(t, true, true)
		os.Remove("testfile/created_magic.jpg")
	}

	if _, err := os.Stat("testfile/created_magic.jpeg"); os.IsNotExist(err) {
		assert.Equal(t, true, false)
	} else {
		assert.Equal(t, true, true)
		os.Remove("testfile/created_magic.jpeg")
	}

	if _, err := os.Stat("testfile/created_magic.png"); os.IsNotExist(err) {
		assert.Equal(t, true, false)
	} else {
		assert.Equal(t, true, true)
		os.Remove("testfile/created_magic.png")
	}
}
