/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"image"
	"image/color"
	"sync"
)

// Get gray value of an grayscale color as integer
func GetGrayValue(col color.Color) int {
	r, _, _, _ := col.RGBA()
	return int(r / 256)
}

// Convert an grayscale image to an two dimensional array
func ImageToTwoDimArray(img image.Image) [][]int {
	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	imgArray := make([][]int, columns)

	for i := range imgArray {
		imgArray[i] = make([]int, rows)
	}

	var wg sync.WaitGroup

	for row := 0; row < rows; row++ {
		wg.Add(1)
		go func(row int) {
			for column := 0; column < columns; column++ {
				imgArray[column][row] = GetGrayValue(img.At(column, row))
			}
			wg.Done()
		}(row)
	}

	return imgArray
}

// Convert an two dimensional array to an grayscale image
func TwoDimArrayToImage(imgArray [][]int) image.Image {
	columns := len(imgArray)
	rows := len(imgArray[0])

	rect := new(image.Rectangle)
	rect.Max.X = columns
	rect.Max.Y = rows

	img := image.NewGray(*rect)

	for row := 0; row < rows; row++ {
		for column := 0; column < columns; column++ {
			grayColor := color.Gray{Y: uint8(imgArray[column][row])}
			img.Set(column, row, grayColor)
		}
	}

	return img
}
