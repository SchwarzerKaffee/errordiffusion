/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type BenchmarkDiffuserMock struct {
	diffuseFloydCalled  bool
	diffuseStuckiCalled bool
	diffuseOwnCalled    bool
}

func (bd *BenchmarkDiffuserMock) Diffuse(imgArray [][]int, matrix [][]float64, rows int, columns int) {
	if TwoDimFloat64ArrayEquals(matrix, floydMatrix) {
		bd.diffuseFloydCalled = true
	}

	if TwoDimFloat64ArrayEquals(matrix, stuckiMatrix) {
		bd.diffuseStuckiCalled = true
	}

	if TwoDimFloat64ArrayEquals(matrix, ownMatrix) {
		bd.diffuseOwnCalled = true
	}
}

func TwoDimFloat64ArrayEquals(a [][]float64, b [][]float64) bool {
	for i, _ := range a {
		for j, _ := range a[i] {
			if a[i][j] != b[i][j] {
				return false
			}
		}
	}

	return true
}

func TestCreateBenchmark(t *testing.T) {
	// Prepare
	var bd *BenchmarkDiffuserMock = new(BenchmarkDiffuserMock)
	var b Benchmark
	img, _ := LoadImage("testfile/test.jpg")

	// Execute
	b.CreateBenchmark(bd, img, "testfile/")

	// Verify
	assert.Equal(t, true, bd.diffuseFloydCalled)
	assert.Equal(t, true, bd.diffuseStuckiCalled)
	assert.Equal(t, true, bd.diffuseOwnCalled)
}
