/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateGrayScaleImage(t *testing.T) {
	// Prepare
	img, _ := LoadImage("testfile/test.jpg")

	// Execute
	grayImg := GenerateGrayScaleImage(img)

	// Verify
	r, g, b, _ := grayImg.At(5, 11).RGBA()
	assert.Equal(t, r, g)
	assert.Equal(t, g, b)
	assert.Equal(t, 52171, int(r))

	r, g, b, _ = img.At(5, 11).RGBA()
	assert.NotEqual(t, r, g)
	assert.NotEqual(t, g, b)

	r, g, b, _ = grayImg.At(99, 11).RGBA()
	assert.Equal(t, r, g)
	assert.Equal(t, g, b)
	assert.Equal(t, 54998, int(r))

	r, g, b, _ = img.At(99, 11).RGBA()
	assert.NotEqual(t, r, g)
	assert.NotEqual(t, g, b)
	assert.Equal(t, 53203, int(r))
}
