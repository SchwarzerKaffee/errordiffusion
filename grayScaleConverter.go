/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"image"
	"image/color"
)

// Create a grayscale image based on input image and return a new one
func GenerateGrayScaleImage(img image.Image) image.Image {
	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y
	grayImg := image.NewGray(bounds)

	for row := 0; row < rows; row++ {
		for column := 0; column < columns; column++ {
			fullColor := img.At(column, row)
			grayColor := color.GrayModel.Convert(fullColor)
			grayImg.Set(column, row, grayColor)
		}
	}

	return grayImg
}
