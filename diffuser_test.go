/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"fmt"
	"image"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TwoDimImageEquals(a image.Image, b image.Image) bool {
	for x := 0; x <= a.Bounds().Max.X; x++ {
		for y := 0; y <= a.Bounds().Max.Y; y++ {
			if a.At(x, y) != b.At(x, y) {
				r1, g1, b1, _ := a.At(x, y).RGBA()
				r2, g2, b2, _ := b.At(x, y).RGBA()
				fmt.Printf("R:%d G:%d B:%d | R:%d G:%d B:%d", r1, g1, b1, r2, g2, b2)
				fmt.Printf(" at %d.%d", x, y)
				fmt.Println()
				return false
			}
		}
	}

	return true
}

func TestDiffuseFloyd(t *testing.T) {
	// Prepare
	var d Diffuser
	imgNormal, _ := LoadImage("testfile/test.jpg")
	img := GenerateGrayScaleImage(imgNormal)
	imgArray := ImageToTwoDimArray(img)
	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	// Execute
	d.Diffuse(imgArray, floydMatrix, rows, columns)
	SaveImage(TwoDimArrayToImage(imgArray), "testfile/test.jpg", "floyd")

	// Verify
	imgResult, _ := LoadImage("testfile/result/test_floyd.jpg")
	imgCalculated, _ := LoadImage("testfile/test_floyd.jpg")
	equalArrays := TwoDimImageEquals(imgResult, imgCalculated)
	assert.Equal(t, true, equalArrays)
}

func TestDiffuseStucki(t *testing.T) {
	// Prepare
	var d Diffuser
	imgNormal, _ := LoadImage("testfile/test.jpg")
	img := GenerateGrayScaleImage(imgNormal)
	imgArray := ImageToTwoDimArray(img)
	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	// Execute
	d.Diffuse(imgArray, stuckiMatrix, rows, columns)
	SaveImage(TwoDimArrayToImage(imgArray), "testfile/test.jpg", "stucki")

	// Verify
	imgResult, _ := LoadImage("testfile/result/test_stucki.jpg")
	imgCalculated, _ := LoadImage("testfile/test_stucki.jpg")
	equalArrays := TwoDimImageEquals(imgResult, imgCalculated)
	assert.Equal(t, true, equalArrays)
}

func TestDiffuseOwn(t *testing.T) {
	// Prepare
	var d Diffuser
	imgNormal, _ := LoadImage("testfile/test.jpg")
	img := GenerateGrayScaleImage(imgNormal)
	imgArray := ImageToTwoDimArray(img)
	bounds := img.Bounds()
	columns := bounds.Max.X
	rows := bounds.Max.Y

	// Execute
	d.Diffuse(imgArray, ownMatrix, rows, columns)
	SaveImage(TwoDimArrayToImage(imgArray), "testfile/test.jpg", "own")

	// Verify
	imgResult, _ := LoadImage("testfile/result/test_own.jpg")
	imgCalculated, _ := LoadImage("testfile/test_own.jpg")
	equalImages := TwoDimImageEquals(imgResult, imgCalculated)
	assert.Equal(t, true, equalImages)
}
