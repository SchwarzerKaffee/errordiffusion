/*
David Rohrer - 3154839
Marius Rothenbuecher - 1040418
*/
package main

import (
	"sync"
)

type Diffuser struct {
}

type IDiffuser interface {
	Diffuse(imgArray [][]int, matrix [][]float64, rows int, columns int)
}

// Prepare diffusing
func (d Diffuser) Diffuse(imgArray [][]int, matrix [][]float64, rows int, columns int) {
	var wg sync.WaitGroup
	wg.Add(rows)

	channels := make([]chan int, rows)

	for row := 0; row < rows; row++ {
		channels[row] = make(chan int)
	}

	for row := 0; row < rows; row++ {
		go DiffuseWork(imgArray, matrix, rows, columns, row, channels, &wg)
	}

	wg.Wait()
}

// Execute diffusing algorithm
func DiffuseWork(imgArray [][]int, matrix [][]float64, rows int, columns int, row int, channels []chan int, wg *sync.WaitGroup) {
	columnMargin := 25
	columnToCalculate := -1

	// Matrix specific
	columnDifference := GetColumnDifference(matrix)

	if row == 0 {
		columnToCalculate = columns - 1
	}

	for column := 0; column < columns; column++ {
		for columnToCalculate < column {
			if column >= columns-1-columnDifference {
				columnToCalculate += columnDifference
			} else {
				columnToCalculate = <-channels[row]

			}
		}

		oldPixel := imgArray[column][row]
		newPixel := 0

		if oldPixel >= 128 {
			imgArray[column][row] = 255
			newPixel = 255
		} else {
			imgArray[column][row] = 0
		}

		error := oldPixel - newPixel

		for matrixRow := 0; matrixRow < len(matrix); matrixRow++ {
			for matrixColumn := 0; matrixColumn < len(matrix[0]); matrixColumn++ {
				matrixValue := matrix[matrixRow][matrixColumn]

				if matrixValue != 0 && matrixValue != 1 {
					targetColumn := column + matrixColumn - columnDifference
					targetRow := row + matrixRow

					if targetRow <= rows-1 && targetColumn <= columns-1 {
						if targetRow >= 0 && targetColumn >= 0 {
							imgArray[targetColumn][targetRow] += int(float64(error) * matrixValue)
						}
					}
				}
			}
		}

		if row != rows-1 && (column%columnMargin == 0 || columns-1 == column) {
			channels[row+1] <- column - 1 - columnDifference
		}
	}

	wg.Done()
}
